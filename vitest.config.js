import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { ViteEjsPlugin } from 'vite-plugin-ejs';
import path from 'path';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [ViteEjsPlugin(), vue()],
  test: {
    globals: true,
    coverage: {
      provider: 'istanbul'
    },
    environment: 'happy-dom'
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  }
});
