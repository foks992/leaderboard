import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { ViteEjsPlugin } from 'vite-plugin-ejs';
import path from 'path';

export default defineConfig({
  plugins: [ViteEjsPlugin(), vue()],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src')
    }
  },
  optimizeDeps: {
    include: ['fast-deep-equal'],
    esbuildOptions: {
      target: 'es2022'
    }
  },
  build: {
    target: 'es2022'
  },
  esbuild: {
    target: 'es2022'
  }
});
