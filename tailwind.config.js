/** @type {import('tailwindcss').Config} */
import defaultTheme from 'tailwindcss/defaultTheme';
import plugin from 'tailwindcss/plugin';

module.exports = {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        darkwhite: '#F9FAFB',
        lightgrey: '#F3F4F6',
        semigrey: '#B6B6B6',
        grey: '#6B7280',
        hgrey: '#4B5563',
        darkgrey: '#374151',
        blue: '#6895ED',
        green: '#29C926'
      },
      fontFamily: {
        sans: ['Inter', ...defaultTheme.fontFamily.sans],
        inter: ['Inter', ...defaultTheme.fontFamily.sans]
      }
    }
  },
  plugins: [
    plugin(function ({ addBase }) {
      addBase({});
    })
  ]
};
