import { createApp } from 'vue';
import { createPinia } from 'pinia';
import App from './App.vue';
import router from './router';
import axios from 'axios';
import VueAxios from 'vue-axios';

import './assets/styles/style.css';

const app = createApp(App);
const pinia = createPinia();

app.use(VueAxios, axios).use(router).use(pinia).mount('#app');
