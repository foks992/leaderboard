export type Payload = {
  [key: string]: any;
};

export enum EQuestStatus {
  Complete = 'Complete',
  Bronze = 'Bronze',
  Silver = 'Silver',
  Gold = 'Gold'
}

export type TQuestStatus =
  | EQuestStatus.Bronze
  | EQuestStatus.Silver
  | EQuestStatus.Gold
  | EQuestStatus.Complete;

export interface Player {
  username: string;
  rank: number;
  quest_1: TQuestStatus;
  quest_2: TQuestStatus;
  quest_3: TQuestStatus;
  quest_4: TQuestStatus;
  quest_5: TQuestStatus;
  quest_6: TQuestStatus;
  quest_7: TQuestStatus;
}
