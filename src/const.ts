export const environments = {
  development: 'development',
  production: 'production'
};

export const environment = import.meta.env.MODE;

export const baseURL = '/';

export const route_names = {
  app: 'app'
};
