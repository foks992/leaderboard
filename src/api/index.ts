import axios, { type AxiosInstance } from 'axios';
import type { Payload, Player } from '@/types';
import { baseURL } from '@/const';

export const axiosDefaultOptions = {
  baseURL,
  headers: {
    common: {
      Accept: 'application/json',
      'Content-type': 'application/json'
    }
  },
  timeout: 10000, // ms
  withCredentials: true,
  enableSpinner: true
};

const setupInterceptors = (instance: AxiosInstance): AxiosInstance => {
  // instance.interceptors.request.use();
  // instance.interceptors.response.use();
  return instance;
};

export const API = setupInterceptors(axios.create(axiosDefaultOptions));

// API helpers
export const apiHelpers = {
  get(endpoint: string) {
    return API.get(endpoint).then((response) => {
      if (!import.meta.env.PROD) {
        console.log('Got response for GET', endpoint, ': ', response.data);
      }
      return response.data;
    });
  },
  patch(endpoint: string, data: Payload) {
    return API.patch(endpoint, data).then((response) => {
      if (!import.meta.env.PROD) {
        console.log('Got response for PATCH', endpoint, ': ', response.data);
      }
      return response.data;
    });
  },
  post(endpoint: string, data: Payload) {
    return API.post(endpoint, data).then((response) => {
      if (!import.meta.env.PROD) {
        console.log('Got response for POST', endpoint, ': ', response.data);
      }
      return response.data;
    });
  },
  delete(endpoint: string) {
    return API.delete(endpoint).then((response) => {
      if (!import.meta.env.PROD) {
        console.log('Got response for DELETE', endpoint, ': ', response.data);
      }
      return response.data;
    });
  }
};

// API requests
export const api = {
  getPlayers(): Promise<Player[]> {
    return API.get('/api/players/');
  }
};
