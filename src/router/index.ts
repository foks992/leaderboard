import { createRouter, createWebHistory } from 'vue-router';
import type { RouteLocationNormalized } from 'vue-router';
import { route_names } from '@/const';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: route_names.app,
      components: {
        content: () => import('../pages/LeaderBoardPage.vue')
      },
      meta: {
        title: 'Leaderboard'
      }
    }
  ],
  scrollBehavior() {
    document.getElementById('app')!.scrollIntoView({ behavior: 'auto' });
  }
});

router.beforeEach(
  (to: RouteLocationNormalized, from: RouteLocationNormalized): any => {
    document.title = (to.meta.title as string) || '';
  }
);

export default router;
