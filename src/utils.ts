import { EQuestStatus, type Player } from '@/types';

export function generateRandomPlayersArray(n: number): Player[] {
  const mockArray = [];
  const statuses = [
    EQuestStatus.Bronze,
    EQuestStatus.Silver,
    EQuestStatus.Gold,
    EQuestStatus.Complete
  ];
  const ranks = Array.from({ length: n }, (_, i) => i + 1);

  // Shuffle ranks array
  for (let i = ranks.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [ranks[i], ranks[j]] = [ranks[j], ranks[i]];
  }

  function getRandomTeam() {
    const teams = ['@MyAmazingTeam', '@AnotherTeam'];
    return teams[Math.floor(Math.random() * teams.length)];
  }

  // Generate player objects
  for (let i = 0; i < n; i++) {
    const player = {
      username: getRandomTeam(),
      rank: ranks[i],
      quest_1: statuses[Math.floor(Math.random() * statuses.length)],
      quest_2: statuses[Math.floor(Math.random() * statuses.length)],
      quest_3: statuses[Math.floor(Math.random() * statuses.length)],
      quest_4: statuses[Math.floor(Math.random() * statuses.length)],
      quest_5: statuses[Math.floor(Math.random() * statuses.length)],
      quest_6: statuses[Math.floor(Math.random() * statuses.length)],
      quest_7: statuses[Math.floor(Math.random() * statuses.length)]
    };
    mockArray.push(player);
  }

  return mockArray;
}
