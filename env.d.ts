/// <reference types="vite/client" />

type AppMethods = {};

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties extends AppMethods {
    $root: AppMethods;
  }
}

export {}; // TS module augmentation works correctly only when it is placed in a module.
